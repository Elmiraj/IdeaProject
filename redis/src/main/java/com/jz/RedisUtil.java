package com.jz;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * RedisUtil class
 *
 * @author zhangjun
 * @date 21/03/2018
 */
public class RedisUtil {

    private static String ADDR = "localhost";
    private static int PORT = 6379;
    /**
     * MAX_ACTIVE
     * Enabled max connection amount, default number is 8
     * If the given value is -1, it means no limited; if the pool has
     * distributed maxActive jedis living example, the condition of
     * the pool is exhausted.
     */
    private static int MAX_ACTIVE = -1;
    /**
     * MAX_IDLE
     * The max idel of jedis living example, the default value is 8
     */
    private static int MAX_IDLE = 8;
    /**
     * MAX_WAIT
     * Max waiting time, default value is -1, means no overtime, else throws JedisConnectionException
     */
    private static int MAX_WAIT = 10000;
    /**
     * TIMEOUT
     */
    private static int TIMEOUT = 10000;

    /**
     * TEST_ON_BORROW
     * Borrow a jedis living example, judge that whether it should be done the validate process, if it is true,
     * return an usable jedis living example
     */
    private static boolean TEST_ON_BORROW = true;


    private static JedisPool jedisPool;

    static {
        try{
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(MAX_ACTIVE);
            config.setMinIdle(MAX_IDLE);
            config.setMaxWaitMillis(MAX_WAIT);
            config.setTestOnBorrow(TEST_ON_BORROW);
            jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public synchronized static Jedis getJedis(){
        Jedis jedis = null;
        if(jedisPool != null){
            jedis = jedisPool.getResource();
        }
        return jedis;
    }
}
