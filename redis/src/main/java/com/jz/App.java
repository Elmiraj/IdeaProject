package com.jz;
import redis.clients.jedis.Jedis;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * App class
 *
 * @author zhangjun
 * @date 19/03/2018
 */
public class App 
{

    public static void main( String[] args )
    {
//        connect to redis
        Jedis jedis = new Jedis("localhost");
        System.out.println("Success");
        System.out.println( "Processing: \n" + jedis.ping() );
//        redis java String
        jedis.set("studentkey", "zj");
        System.out.println("redis store String: " + jedis.get("studentkey"));
//        redis java List
        jedis.lpush("site-list", "www.google.com");
        jedis.lpush("site-list", "www.facebook.com");
        List<String> list = jedis.lrange("site-list", 0 , 1);
        for(int i=0; i<list.size(); i++){
            System.out.println("List: " + list.get(i));
        }
//        redis java keys
        Set<String> keys = jedis.keys("*");
        Iterator<String> it = keys.iterator();
        while(it.hasNext()){
            String key = it.next();
            System.out.println(key);
        }
    }
}
