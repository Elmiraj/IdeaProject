package com.jz;

import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.*;

import java.util.LinkedList;
import java.util.List;

public class TestRedisPool {
    private JedisPool jedisPool;
//    private Jedis jedis;
    private JedisShardInfo jedisShardInfo;
    private JedisPoolConfig jedisPoolConfig;

    @Before
    public void setup(){
//        jedis = new Jedis("localhost");
        jedisPool = new JedisPool("localhost");
        jedisShardInfo = new JedisShardInfo("localhost");
        jedisPoolConfig = new JedisPoolConfig();
    }

    @Test
    public void testJedisPool(){
        Jedis jedis = jedisPool.getResource();
        String keys = "name";
        String value = "zj";

        jedis.del(keys);
        jedis.set(keys,value);

        System.out.println(value);
        jedisPool.close();
    }

    @Test
    public void testJedisSharedPool(){
        List<JedisShardInfo> list = new LinkedList<>();
        list.add(jedisShardInfo);
        ShardedJedisPool shardedJedisPool = new ShardedJedisPool(jedisPoolConfig,list);
        ShardedJedis shardedJedis = shardedJedisPool.getResource();

        String keys1 = "name";
        String value1 = "zj";

        shardedJedis.del(keys1);
        shardedJedis.set(keys1,value1);
        System.out.println(shardedJedis.get(keys1));

        shardedJedis.del("keys1");
        shardedJedis.lpush("keys1","value1");
        shardedJedis.lpush("keys1","value2");
        shardedJedis.lpush("keys1","value3");
        shardedJedis.lpush("keys1","value4");
        System.out.println(shardedJedis.lrange("keys1",0,-1));

        shardedJedis.close();
    }
}
