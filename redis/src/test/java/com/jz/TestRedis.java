package com.jz;

import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TestRedis {
    private Jedis jedis;

    @Before
    public void setup(){
//        connect to redis server
        jedis = new Jedis("localhost");
    }

    @Test
    public void testString(){
        jedis.set("name", "zj");
        System.out.println(jedis.get("name"));

        jedis.append("name", " is a man");
        System.out.println(jedis.get("name"));

        jedis.del("name");
        System.out.println(jedis.get("name"));

        jedis.mset("name", "zj", "age","24", "gender", "male", "regNum", "123");
        jedis.incr("age");
        jedis.decr("regNum");
        System.out.println(jedis.get("name") + " with the age of " + jedis.get("age") + ", is a " + jedis.get("gender")
        + ", " + "registration number is " + jedis.get("regNum"));

        System.out.println(jedis.strlen("name"));

        jedis.set("name","lh");
        System.out.println(jedis.get("name"));
    }

    @Test
    public void testHash(){
        jedis.hset("stu","name","zj");
        System.out.println(jedis.hkeys("stu"));
        System.out.println(jedis.hvals("stu"));

        Map<String, String> map = new HashMap<>();
        map.put("name","zj");
        map.put("age","24");
        map.put("gender", "male");
        jedis.hmset("user",map);

        List<String> list = jedis.hmget("user","name", "age", "gender");
        System.out.println(list);
//        delete some key in map
        jedis.hdel("user", "age");
//        return null because of deleted
        System.out.println(jedis.hmget("user", "age") + "there is no field named age");
//        return the number of keys in user
        System.out.println("The number of keys in user is: " + jedis.hlen("user"));
//        judge whether there is a key named user
        System.out.println("Is there any keys named user? " + jedis.exists("user"));
//        return keys in map object
        System.out.println("Exist keys: " + jedis.hkeys("user"));
//        return values in map object
        System.out.println("Exist values: " + jedis.hvals("user"));

        jedis.hset("user","name","lh");
        Iterator<String> iterator = jedis.hkeys("user").iterator();

        /**
         * Three loop methods
         * */
        for (String key: jedis.hkeys("user")
             ) {
            System.out.println(key + ":" + jedis.hmget("user", key));
        } 

        for(Iterator<String> it = jedis.hkeys("user").iterator(); it.hasNext();){
            String key = it.next();
            System.out.println(key + ":" + jedis.hmget("user", key));
        }

        while(iterator.hasNext()){
            String key = iterator.next();
            System.out.println(key + ":" + jedis.hmget("user", key));
        }
    }

    @Test
    public void testList(){
        jedis.del("java framework");
        System.out.println(jedis.lrange("jave framework", 0, -1));
//        store 3 data in the key named java framework
        jedis.lpush("java framework", "spring");
        jedis.lpush("java framework", "struts");
        jedis.lpush("java framework", "hibernate");
//        output all data
        System.out.println(jedis.lrange("java framework", 0, -1));

        jedis.del("java framework");
        jedis.rpush("java framework", "spring");
        jedis.rpush("java framework", "struts");
        jedis.rpush("java framework", "hibernate");
        System.out.println(jedis.lrange("java framework", 0, -1));

        jedis.lrem("java framework",0, "spring");
        jedis.lset("java framework", 1, "maven");
        System.out.println(jedis.lrange("java framework", 0, -1));
    }

    @Test
    public void testSet(){
//        add members into student
        jedis.sadd("student", "zj");
        jedis.sadd("student", "lh");
        jedis.sadd("student", "tyq");
        jedis.sadd("student", "jj");
//        delete one member
        jedis.srem("student", "zj");
        System.out.println("Get all input value: " + jedis.smembers("student"));
        System.out.println("Judge whether 'zj' is the element of student: " + jedis.sismember("student", "zj"));
        System.out.println("Return random student value: " + jedis.srandmember("student"));
        System.out.println("Return all elements in the student group: " + jedis.scard("student"));
    }

    @Test
    public void testSorted(){
        jedis.del("a");
        jedis.rpush("a", "1");
        jedis.lpush("a", "9");
        jedis.lpush("a", "6");
        jedis.lpush("a", "7");

        System.out.println(jedis.lrange("a", 0, -1));
        System.out.println("Sorted: " + jedis.sort("a"));
    }

    @Test
    public void testPool(){
        RedisUtil.getJedis().set("new_name","zj");
        System.out.println(RedisUtil.getJedis().get("new_name"));
    }
}
