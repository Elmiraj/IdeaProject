package com.jz;

public class Student {
    private int stuid;
    private String stuname;
    private String gender;

    public int getStuid() {
        return stuid;
    }

    public void setStuid(int stuid) {
        this.stuid = stuid;
    }

    public String getStuname() {
        return stuname;
    }

    public void setStuname(String stuname) {
        this.stuname = stuname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (stuid != student.stuid) return false;
        if (stuname != null ? !stuname.equals(student.stuname) : student.stuname != null) return false;
        if (gender != null ? !gender.equals(student.gender) : student.gender != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = stuid;
        result = 31 * result + (stuname != null ? stuname.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        return result;
    }
}
