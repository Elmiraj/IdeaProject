package com.jz;

import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        App app = new App();
        app.createAndStore();
        HibernateUtil.sessionFactory.close();
    }

    private void createAndStore(){
        Session session = HibernateUtil.currentSession();
        Transaction transaction = session.beginTransaction();

        Student student = new Student();
        student.setStuid(1);
        student.setStuname("das");
        student.setGender("boy");
        session.save(student);
        transaction.commit();
        HibernateUtil.closeSession();
    }
}
